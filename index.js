import express from "express";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
import cors from "cors";
import router from "./route/index.js";
import connectToDB from "./config/DatabaseMongo.js";
dotenv.config();
const app = express();

app.use(cors({ credentials:true, origin:'http://localhost:3005' }));
app.use(cookieParser());
app.use(express.json());
app.use(router);

app.get("/", (req, res) => {
    res.json({ message: "Hiiii!!!!" });
});

(async () => {
    await connectToDB();
})();

app.listen(5000, ()=> console.log('Server running at port 5000'));