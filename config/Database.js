import { Sequelize } from "sequelize";
import dotenv from "dotenv";

dotenv.config();

const db = new Sequelize(process.env.DBNAME, process.env.DBUSER, process.env.DBPASSWORD, {
    host: process.env.DBHOST,
    dialect: "mysql",
    define: {
        timestamps: false
    },
    // logging: false
});

// Migrate the database
db.sync() // set force to true to drop existing tables and recreate them
.then(() => {
    console.log('Database migrated successfully.');
})
.catch((error) => {
    console.error('Error migrating database:', error);
});

export default db;