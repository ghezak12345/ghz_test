import { Types, CastError } from "mongoose";
import Books from "../models/Book.js";
import { getPagination } from "../utils/pagination.js";

export const getBooks = async(req, res) => {
    try {
        let { page, perPage, search, sort, sortColumn } = req.body;

        page = (!page ? 1 : page );
        perPage = (!perPage ? 10 : perPage );
        
        let { limit, skip } = getPagination(page, perPage);
        
        var condition = { deleted: 'N' }

        if(search){
            condition = Object.assign(condition, { 
                full_name: { $regex: search, $options: 'i' } 
            });
        }
        
        sortColumn = (!sortColumn ? 'updated_at' : sortColumn );
        sort = (!sort ? 'desc' : sort );

        const books = await Books.find(condition)
            .sort({ [sortColumn]: sort })
            .skip(skip)
            .limit(limit);

        const total = await Books.countDocuments(condition);
        
        res.json({books : books, total : total});
    } catch (error) {
        res.status(500).json({msg:"Server Error please try again later!!! "});
    }
}

export const createBooks = async(req, res) => {
    try {
        const insertBooks = await Books.insertBooks(req.body, req);
        if(insertBooks.status){
            res.json({ success: insertBooks.status, msg: insertBooks.msg });
        }else{
            res.status(500).json({ msg: insertBooks.msg });
        }
    } catch (error) {
        res.status(500).json({msg:"Server Error please try again later!!! "});
    }
}

export const getBooksOne = async(req, res) => {
    try {
        const book = await Books.findById(req.body.id);
        res.json({ success: true, data: book });
    } catch (error) {
        res.status(500).json({msg:"Data Not Found!! "});
    }
}

export const updateBooks = async(req, res) => {
    try {
        const updateBooks = await Books.updateBooks(req.body, req);
        if(updateBooks.status){
            res.json({ success: updateBooks.status, msg: updateBooks.msg });
        }else{
            res.status(500).json({ msg: updateBooks.msg });
        }
    } catch (error) {
        res.status(500).json({msg:"Server Error please try again later!!! "});
    }
}

export const deleteBooks = async(req, res) => {
    try {
        const deleteBooks = await Books.deleteBooks(req.body, req);
        if(deleteBooks.status){
            res.json({ success: deleteBooks.status, msg: deleteBooks.msg });
        }else{
            res.status(500).json({ msg: deleteBooks.msg });
        }
    } catch (error) {
        res.status(500).json({msg:"Server Error please try again later!!! "});
    }
}
