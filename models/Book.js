import moment from "moment-timezone";
import mongoose from "mongoose";
import { getDataToken } from "../utils/getDataToken.js";

const BookSchema = new mongoose.Schema({
    tittle: String,
    total_pages: String,
    rating: String,
    authors: String,
    genres: String,
    published: String,
    deleted: {
        type: String,
        default: 'N'
    },
    created_by: String,
    updated_by: String,
    created_at: {
        type: String,
        default: moment().tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')
    },
    updated_at: {
        type: String,
        default: moment().tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')
    }
}, { versionKey: false });

const Books = mongoose.model('Books', BookSchema);

Books.insertBooks = async function (request, headers) {
    try {
        let data = getDataToken(headers);
        
        let newInsert;
        newInsert = new Books({
            tittle: request.title,
            total_pages: request.total_pages,
            rating: request.rating,
            authors: request.authors,
            genres: request.genres,
            published: request.published,
            created_by: data.userId,
            updated_by: data.userId,
        });
        
        await newInsert.save();
        
        return {status: true, msg: 'Insert Books Successfully!!!'}
    } catch (error) {
        console.log(error)
        throw Error(error.message)
    }
}

Books.updateBooks = async function (request, headers) {
    try {
        let data = getDataToken(headers);

        await Books.updateOne(
            {
                _id: request.id
            }, 
            { 
                $set: { 
                    tittle: request.title,
                    total_pages: request.total_pages,
                    rating: request.rating,
                    authors: request.authors,
                    genres: request.genres,
                    published: request.published,
                    updated_by: data.userId,
                    updated_at: moment().tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')
                } 
            }
        );
        
        return {status: true, msg: 'Update Books Successfully!!!'}
    } catch (error) {
        return {status: true, msg: 'Update Books Failed, Data Not Found!!!'}
    }
}

Books.deleteBooks = async function(request,headers) {
    try {
        let data = getDataToken(headers);

        await Books.updateOne(
            {
                _id: request.id
            }, 
            { 
                $set: { 
                    deleted: 'Y',
                    updated_by: data.userId,
                    updated_at: moment().tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')
                } 
            }
        );
        
        return {status: true, msg: 'Delete Books Successfully!!!'}
    } catch (error) {
        return {status: true, msg: 'Delete Books Failed, Data Not Found!!!'}
    }
};

export default Books;