import express from "express";
import { createBooks, deleteBooks, getBooks, getBooksOne, updateBooks } from "../../controller/BookController.js";
import { verifyToken } from "../../middleware/VerifyToken.js";

const router = express.Router();

router.use(verifyToken)
router.post('/books', getBooks);
router.post('/onebooks', getBooksOne);
router.post('/createbooks', createBooks);
router.post('/updatebooks', updateBooks);
router.post('/deletebooks', deleteBooks);

export default router;