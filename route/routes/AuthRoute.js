import express from "express";
import { Login, Logout, refreshToken } from "../../controller/AuthController.js";

const router = express.Router();

router.post('/auth/login', Login);
router.get('/auth/token', refreshToken);
router.get('/auth/logout', Logout);

export default router;