// ** Navigation imports
import authenticate from './routes/AuthRoute.js'
import book from './routes/BookRoutes.js'

// ** Merge & Export
export default [authenticate, book]
