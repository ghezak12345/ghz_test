import jwt from "jsonwebtoken";

export const getDataToken = (req) => {
    const accessToken = req.cookies.accessToken;
    let token = null;
    if(accessToken){
        token = accessToken;
    }else{
        const authHeader = req.headers['authorization'];
        token = authHeader && authHeader.split(' ')[1];
    }
    let data = null;
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
        if(err) return {status: false, msg: 'Token has been expired!!!' };
        data = decoded;
    })
    return data;
};